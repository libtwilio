#ifndef __TWLILO_H__
#define __TWLILO_H__

#include <time.h>
#include <curl/curl.h>
#include <libex/str.h>
#include <libex/json.h>

typedef struct {
    int is_success;
} twilio_result_t;

typedef struct {
    int is_success;
    int code;
    cstr_t *message;
    cstr_t *more_info;
    int status;
} twilio_error_t;

typedef struct {
    int is_success;
    time_t created;
    time_t updated;
    time_t sent;
    cstr_t *price;
    cstr_t *price_unit;
    cstr_t *status;
    cstr_t *uri;
    cstr_t *sid;
} twilio_success_t;

void twilio_init (const char *sid, const char *token);
twilio_result_t *twilio_send_sms (const char *from, const char *to, const char *body);
long twilio_update_result (twilio_success_t *x);
void twilio_free_result (twilio_result_t *result);

#endif
