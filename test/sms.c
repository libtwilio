#include <unistd.h>
#include "twilio.h"

int main (int argc, const char *argv[]) {
    if (argc < 6) {
        printf("usage: sms <sid> <token> <from> <to> <body>\n");
        return 1;
    }
    const char *sid = argv[1],
               *token = argv[2],
               *from = argv[3],
               *to = argv[4],
               *body = argv[5];
    curl_global_init(CURL_GLOBAL_ALL);
    twilio_init(sid, token);
    twilio_result_t *result = twilio_send_sms(from, to, body);
    if (result->is_success) {
        twilio_success_t *s = (twilio_success_t*)result;
        int i = 10;
        while (i > 0 && 0 != cmpstr(s->status->ptr, s->status->len, CONST_STR_LEN("delivered"))) {
            sleep(1);
            twilio_update_result(s);
        }
        printf("date created: %lu\n", s->created);
        printf("date updated: %lu\n", s->updated);
        printf("date sent: %lu\n", s->sent);
        printf("price: %s\n", s->price ? s->price->ptr : "null");
        printf("price_unit: %s\n", s->price_unit->ptr);
        printf("status: %s\n", s->status->ptr);
        printf("uri: %s\n", s->uri->ptr);
        printf("sid: %s\n", s->sid->ptr);
    } else {
        twilio_error_t *e = (twilio_error_t*)result;
        printf("code: %d\n", e->code);
        printf("message: %s\n", e->message->ptr);
        printf("more_info: %s\n", e->more_info ? e->more_info->ptr : "null");
        printf("status: %d\n", e->status);
    }
    twilio_free_result(result);
    curl_global_cleanup();
    return 0;
}
