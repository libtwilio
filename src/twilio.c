#include "twilio.h"

#define TWILIO_URI "https://api.twilio.com/2010-04-01/Accounts/%s/Messages.json"
#define TWILIO_HOST "https://api.twilio.com"
#define TWILIO_TIME "%a, %d %b %Y %H:%M:%S GMT"

static cstr_t *twilio_sid = NULL;
static cstr_t *twilio_token = NULL;

cstr_t *twilio_uri = NULL;

void twilio_init (const char *sid, const char *token) {
    char buf [128];
    twilio_sid = mkcstr(sid, 0);
    twilio_token = mkcstr(token, 0);
    int len = snprintf(buf, sizeof buf, TWILIO_URI, sid);
    twilio_uri = mkcstr(buf, len);
}

void twilio_free_result (twilio_result_t *result) {
    if (result->is_success) {
        twilio_success_t *s = (twilio_success_t*)result;
        if (s->price)
            free(s->price);
        if (s->price_unit)
            free(s->price_unit);
        if (s->status)
            free(s->status);
        if (s->uri)
            free(s->uri);
        if (s->sid)
            free(s->sid);
    } else {
        twilio_error_t *e = (twilio_error_t*)result;
        if (e->message)
            free(e->message);
        if (e->more_info)
            free(e->more_info);
    }
    free(result);
}

__attribute__ ((destructor))
static void twilio_done () {
    if (twilio_uri)
        free(twilio_uri);
    if (twilio_token)
        free(twilio_token);
    if (twilio_sid)
        free(twilio_sid);
}

static size_t twilio_callback (char *ptr, size_t size, size_t nmemb, str_t **buf) {
    size_t len = size * nmemb;
    strnadd(buf, ptr, len);
    return len;
}

static time_t twilio_totime (json_item_t *ji) {
    time_t res = 0;
    char buf [40] = {};
    if (JSON_STRING == ji->type && ji->data.s.len < sizeof(buf)) {
        struct tm tm;
        memset(&tm, 0, sizeof(tm));
        strncpy(buf, ji->data.s.ptr, ji->data.s.len);
        strptime(buf, TWILIO_TIME, &tm);
        res = mktime(&tm);
    }
    return res;
}

static void twilio_string (json_item_t *ji, cstr_t **str) {
    if (*str) {
        free(*str);
        *str = NULL;
    }
    if (JSON_STRING == ji->type)
        *str = mkcstr(ji->data.s.ptr, ji->data.s.len);
}

static twilio_error_t *twilio_create_error (json_object_t *o) {
    json_item_t *ji;
    twilio_error_t *e = malloc(sizeof(twilio_error_t));
    e->is_success = 0;
    if ((ji = json_find(o, CONST_STR_LEN("code"), JSON_INTEGER)))
        e->code = ji->data.i;
    if ((ji = json_find(o, CONST_STR_LEN("message"), JSON_STRING)))
        e->message = mkcstr(ji->data.s.ptr, ji->data.s.len);
    if ((ji = json_find(o, CONST_STR_LEN("more_info"), JSON_STRING)))
        e->more_info = mkcstr(ji->data.s.ptr, ji->data.s.len);
    if ((ji = json_find(o, CONST_STR_LEN("status"), JSON_INTEGER)))
        e->status = ji->data.i;
    return e;
}

static void twilio_update_success (json_object_t *o, twilio_success_t *x) {
    json_item_t *j;
    if ((j = json_find(o, CONST_STR_LEN("date_created"), JSON_STRING)))
        x->created = twilio_totime(j);
    if ((j = json_find(o, CONST_STR_LEN("date_updated"), JSON_STRING)))
        x->updated = twilio_totime(j);
    if ((j = json_find(o, CONST_STR_LEN("date_sent"), JSON_STRING | JSON_NULL)))
        x->sent = twilio_totime(j);
    if ((j = json_find(o, CONST_STR_LEN("price"), JSON_STRING | JSON_NULL)))
        twilio_string(j, &x->price);
    if ((j = json_find(o, CONST_STR_LEN("price_unit"), JSON_STRING)))
        twilio_string(j, &x->price_unit);
    if ((j = json_find(o, CONST_STR_LEN("status"), JSON_STRING)))
        twilio_string(j, &x->status);
    if ((j = json_find(o, CONST_STR_LEN("uri"), JSON_STRING)))
        twilio_string(j, &x->uri);
    if ((j = json_find(o, CONST_STR_LEN("sid"), JSON_STRING)))
        twilio_string(j, &x->sid);
}

static twilio_success_t *twilio_create_success (json_object_t *o) {
    twilio_success_t *x = calloc(1, sizeof(twilio_success_t));
    x->is_success = 1;
    twilio_update_success(o, x);
    return x;
}

static char *prepare_content (CURL *curl, const char *from, const char *to, const char *body) {
    char *s_from = curl_easy_escape(curl, from, 0),
         *s_to = curl_easy_escape(curl, to, 0),
         *s_body = curl_easy_escape(curl, body, 0),
         *res;
    size_t len = strlen(s_from) + strlen(s_to) + strlen(s_body) + 20;
    res = malloc(len);
    snprintf(res, len, "To=%s&From=%s&Body=%s", s_to, s_from, s_body);
    free(s_body);
    free(s_to);
    free(s_from);
    return res;
}

twilio_result_t *twilio_send_sms (const char *from, const char *to, const char *body) {
    int rc;
    twilio_result_t *result = NULL;
    CURL *curl;
    struct curl_slist *headerlist = NULL;
    char *content;
    str_t *buf;
    curl = curl_easy_init();
    content = prepare_content(curl, from, to, body);
    headerlist = curl_slist_append(headerlist, "Except:");
    headerlist = curl_slist_append(headerlist, "Content-Type: application/x-www-form-urlencoded");
    buf = stralloc(64, 64);
    curl_easy_setopt(curl, CURLOPT_URL, twilio_uri->ptr);
    curl_easy_setopt(curl, CURLOPT_USERNAME, twilio_sid->ptr);
    curl_easy_setopt(curl, CURLOPT_PASSWORD, twilio_token->ptr);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, content);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(content));
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, twilio_callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buf);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);
    if (CURLE_OK == (rc = curl_easy_perform(curl))) {
        long http_code;
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
        if (200 != http_code && 201 != http_code) {
            json_t *json = json_parse_len(buf->ptr, buf->len);
            if (json) {
                if (JSON_OBJECT == json->type)
                    result = (twilio_result_t*)twilio_create_error(json->data.o);
                json_free(json);
            }
        } else {
            json_t *json = json_parse_len(buf->ptr, buf->len);
            if (json) {
                if (JSON_OBJECT == json->type)
                    result = (twilio_result_t*)twilio_create_success(json->data.o);
                json_free(json);
            }
        }
    } else {
        twilio_error_t *e = malloc(sizeof(twilio_error_t));
        const char *curl_error = curl_easy_strerror(rc);
        e->is_success = 0;
        e->code = rc;
        e->message = mkcstr(curl_error, strlen(curl_error));
        e->more_info = NULL;
        e->status = 0;
        result = (twilio_result_t*)e;
    }
    free(buf);
    curl_slist_free_all(headerlist);
    free(content);
    curl_easy_cleanup(curl);
    return result;
}

long twilio_update_result (twilio_success_t *x) {
    long rc;
    CURL *curl = curl_easy_init();
    str_t *uri = mkstr(CONST_STR_LEN(TWILIO_HOST), 128),
          *buf = stralloc(128, 128);
    strnadd(&uri, x->uri->ptr, x->uri->len);
    curl_easy_setopt(curl, CURLOPT_URL, uri->ptr);
    curl_easy_setopt(curl, CURLOPT_USERNAME, twilio_sid->ptr);
    curl_easy_setopt(curl, CURLOPT_PASSWORD, twilio_token->ptr);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, twilio_callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buf);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);
    if (0 == (rc = curl_easy_perform(curl))) {
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &rc);
        if (200 == rc) {
            json_t *json = json_parse_len(buf->ptr, buf->len);
            if (json) {
                if (JSON_OBJECT == json->type)
                    twilio_update_success(json->data.o, x);
                json_free(json);
            }
        }
    }
    free(buf);
    free(uri);
    curl_easy_cleanup(curl);
    return rc;
}
